import Swal from 'sweetalert2'
import { types } from "../types/types";
import { Dispatch } from 'redux'
import { createUserWithEmailAndPassword, signInWithEmailAndPassword , getAuth, onAuthStateChanged, signOut } from "firebase/auth";
import { db, auth} from "../firebase/firebase-config";
import { collection, addDoc, doc, getDocs} from "firebase/firestore";
import { removeCredentialsSpot } from './spotify';
import { loginEndpoint } from '../spotify';
import { endLoading, startLoading } from './loading';

interface userRegister {
    name: string;
    email: string;
    password: string;
}
interface userLogin {
    email: string;
    password: string;
}

export const registerUser = ({name, email, password}: userRegister) => {
    return (dispatch: any) => {
        try {
            createUserWithEmailAndPassword(auth, email, password)
            .then(async (user) => {
                await addDoc(collection(db, "users"), {
                    uid: user.user.uid,
                    name,
                    email,
                    password
                });
                dispatch(loginUser({email, password}));
            })
            .catch((error) => {
                Swal.fire('Error', 'Error during user creation...', 'error');
            });
        } catch {
            Swal.fire('Error', 'Error during user creation...' , 'error');
          } 
    }
}

export const loginUser = ({email, password}: userLogin) => {
    return async(dispatch: Dispatch) => {

        dispatch(startLoading());
        
        await signInWithEmailAndPassword(auth, email, password)
        .then(async () => {
            const usersDb = await getDocs(collection(db, "users"));
            const arrayUsers = usersDb.docs.map((doc: any) => ({...doc.data(), id: doc.id}));
            const userFinded = arrayUsers.find( (user) => user.email === email );
            
                dispatch(loginFinish(userFinded));
                window.location.href = loginEndpoint;
                dispatch(endLoading());
            })
            .catch((error) => {
                Swal.fire('Error', 'Error during login user...', 'error');
                throw error
                dispatch(endLoading());
            });
       
    }
}

export const processLogout = () => {
    return  async (dispatch: Dispatch) => {
        signOut(auth).then(() => {
            localStorage.clear();
            dispatch(removeCredentialsSpot());
            dispatch(removeSongs());
            dispatch(logout());
          }).catch((error) => {
            Swal.fire('Error', 'Error during logout user...', 'error');
          });
    }
}

export const loginFinish = (user: userLogin) => ({
    type: types.authLogin,
    payload: user
});

export const checkLogin = () => {
    return async (dispatch: Dispatch) => {
        
        dispatch(startLoading());
        
        const auth = await getAuth();
        onAuthStateChanged(auth,  async (currentUser) => {
            if (currentUser) {
                const usersDb = await getDocs(collection(db, "users"));
                const arrayUsers = usersDb.docs.map((doc: any) => ({...doc.data(), id: doc.id}));
                const userFinded = arrayUsers.find( (user) => user.uid === currentUser.uid );
                
                dispatch(loginCheckFinish(userFinded));
                dispatch(endLoading());
            } else {
                dispatch(logout());
                dispatch(endLoading());
            }
          });

    }
}

export const loginCheckFinish = (user: userLogin) => ({
    type: types.authChecking,
    payload: user
});

export const logout = () => ({
    type: types.authLogout,
});

export const removeSongs = () => ({
    type: types.removeSongs,
});