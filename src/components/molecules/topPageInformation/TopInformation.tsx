import React from 'react'
import { SubtitlePages } from '../../atoms/subtitlePages/SubtitlePages'
import { TitlePages } from '../../atoms/titlePages/TitlePages'
import './styles.scss';

interface informationPages {
  title: string;
  subtitle: string;
}

export const TopInformation:  React.FC<informationPages> = ( {title, subtitle} ) => {
  return (
    <div className='m-topInformation'>
        <TitlePages titlePage={title} />
        <SubtitlePages subtitlePage={subtitle} 
        />
    </div>
  )
}
