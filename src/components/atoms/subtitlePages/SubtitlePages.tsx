import React from 'react'
import './styles.scss';

interface subtitleProps {
    subtitlePage: string;
}

export const SubtitlePages: React.FC <subtitleProps> = ({ subtitlePage }) => {
  return (
    <h3 className='a-subtitlePage'> { subtitlePage } </h3>
  )
}
