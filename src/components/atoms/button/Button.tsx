import React from 'react'
import "./styles.scss";

interface buttonStyles {
    classButton?: string;
    clickEvent?: any;
    label: string;
    buttonType? : 'submit' | 'reset' | 'button';
}

/**
 * 
 * @param {Object} props - Here are properties of Button 
 * @param {string} props.buttonType - Button type indicates if this is "Button" | "Reset" | "Submit" 
 * @param {string} props.classButton - Class button allow add between 'filled' or 'outlined' styles
 * @param {string} props.label - Label is the name of button 
 * @param {*} props.clickEvent - Click event allow add any functions that do you want it make 
 * 
 * @returns {*} return a Button JSX.
 */

const Button: React.FC<buttonStyles> = ({ buttonType = 'button', label, classButton = 'filled', clickEvent }) => {
  return (
    <button name={label.toLocaleLowerCase()} type={buttonType} className={`a-button ${classButton}`} onClick={clickEvent}>
        <span>{ label }</span>
    </button>
  )
}

export default Button;