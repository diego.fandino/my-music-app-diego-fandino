import React from 'react'
import './styles.scss';

interface imageSongProps {
    url: string;
    name: string;
}

export const ImageSong: React.FC<imageSongProps> = ({url, name}) => {
  return (
    <figure>
      <img className='a-imageSong' src={url} alt={name} />
    </figure>
  )
}
