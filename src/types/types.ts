export const types = {
    authChecking: '[auth] Checking login state',
    authLogin: '[auth] Login',
    authLogout: '[auth] Logout',
    
    startLoading: '[loading] start loading',
    endLoading: '[loading] end loading',
    
    errorMessage: '[message] Show error message',
    removeErrorMessage: '[message] Remove error message',
    
    tokenSpotify: '[token] Token spotify',
    refreshSpotify: '[token] refresh spotify',
    removeSpotifyCredentials: '[credentials] remove spotify credentials',
    checkTokenSpotify: '[token] Check token spotify',
    
    getAllSongOfPlaylist: '[Playlist] Get all songs',
    getFavoriteSongs: '[Playlist] Get all favorite songs',
    removeSongs: '[Playlist] remove songs',
}