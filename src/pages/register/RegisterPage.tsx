import React from 'react'
import { useDispatch } from 'react-redux'
import "./register.scss";
import logoSpotify from "../../assets/logo_spotify_loginAndRegister.png";

import { useNavigate } from "react-router-dom";
import { SubmitHandler, useForm } from "react-hook-form";
import Button from "../../components/atoms/button/Button";
import { Divider } from "../../components/atoms/divider/Divider";
import { registerUser } from "../../actions/auth";

interface IFormRegister {
  name: String;
  email: String;
  password: String;
}

/**
 * 
 * This RegisterPage use:
 * 1. It useNavigate to go to Login page.
 * 2. It use "React hook form" for validate all and allow register to the person
 * 
 */

export const RegisterPage = () => {

  const navigate = useNavigate();
  const dispatch = useDispatch();

  const { register, handleSubmit, formState: { errors } } = useForm<IFormRegister>();
  const onSubmit: SubmitHandler<IFormRegister> = (data: any) => dispatch(registerUser(data));

  const handleLogin = () => {
    navigate("/login");
  }

  return (
    <div className='registerBox'>
      <img src={logoSpotify} alt="logo_spotify" />
        <Divider />
        <form onSubmit={handleSubmit(onSubmit)}>
        <div className='registerBox__input'>
            <label>How do you want us to call you?</label>
            <input aria-label='name' type='text' {...register("name", {
              required: "This field is required",
              })}  placeholder='Add your name' />
            {errors?.name && <span aria-label='ErrorMessages' className='registerBox__input__errorMessage'>{ (errors.name as any).message }</span>}
          </div>
          <div className='registerBox__input'>
            <label>Email address</label>
            <input aria-label='email' type='text' {...register("email", {
              required: "This field is required",
              pattern: {
                value: /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
                message: "Invalid email address"
              }
              })}  placeholder='Email address' />
            {errors?.email && <span aria-label='ErrorMessages' className='registerBox__input__errorMessage'>{ (errors.email as any).message }</span>}
          </div>
          <div className='registerBox__input'>
            <label>Password</label>
            <input  aria-label='password' type='password' {...register("password", {required: {value: true, message: 'This field is required'}})} placeholder='Password' />
            {errors?.password && <span aria-label='ErrorMessages' className='registerBox__input__errorMessage'>{ (errors.password as any).message }</span>}
          </div>
          <Button buttonType='submit' label='Register' classButton='filled'></Button>
        </form>
        
        <Divider />
        <div className='registerBox__register'>
          <h3>Do you already have an account? <span onClick={handleLogin} > Login </span>.</h3>
        </div>
        
    </div>
  )
}
