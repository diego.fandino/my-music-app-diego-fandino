import React from 'react'
import "./login.scss";
import logoSpotify from "../../assets/logo_spotify_loginAndRegister.png";

import { useNavigate } from "react-router-dom";
import { SubmitHandler, useForm } from "react-hook-form";
import Button from "../../components/atoms/button/Button";
import { Divider } from "../../components/atoms/divider/Divider";
import { useDispatch } from 'react-redux';
import { loginUser } from '../../actions/auth';

interface IFormInput {
  email: String;
  password: String;
}

/**
 * 
 * This LoginPage use:
 * 1. It useNavigate to go to Register page.
 * 2. It use "React hook form" for validate all and allow logged to the person
 * 
 */

export const LoginPage = () => {

  const navigate = useNavigate();
  const dispatch = useDispatch();

  const { register, handleSubmit, formState: { errors } } = useForm<IFormInput>();
  const onSubmit: SubmitHandler<IFormInput> = (data: any) => dispatch(loginUser(data));

  const handleRegister = () => {
    navigate("/register");
  }

  return (
    <div className='loginBox'>
        <img src={logoSpotify} alt="logo_spotify" />
        <Divider />
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className='loginBox__input'>
            <label>Email address</label>
            <input aria-label='email' type='text' {...register("email", {
          required: "This field is required",
          pattern: {
            value: /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
            message: "Invalid email address"
          }
        })}  placeholder='Email address' />
            {errors?.email && <span aria-label='email-MsgError' className='loginBox__input__errorMessage'>{(errors.email as any).message }</span>}
          </div>
          <div className='loginBox__input'>
            <label>Password</label>
            <input aria-label='password' type='password' {...register("password", {required: {value: true, message: 'This field is required'}})} placeholder='Password' />
            {errors?.password && <span aria-label='email-MsgError' className='loginBox__input__errorMessage'>{ (errors.password as any).message }</span>}
          </div>
          <Button buttonType='submit' label='Login' classButton='filled'></Button>
        </form>
        
        <Divider />
        <div className='loginBox__register'>
          <h3>Do you not have an account?</h3>
          <Button buttonType='button' label='Register' classButton='outlined' clickEvent={handleRegister}></Button>
        </div>
        
    </div>
  )
}
