import React from 'react'

interface titleProps {
    titlePage: string;
}

export const TitlePages: React.FC<titleProps> = ({ titlePage }) => {
  return (
    <h1 className='a-titlePage'> { titlePage } </h1>
  )
}
