import { types } from "../types/types";

interface messageError {
    message: string
}

export const showErrorMessage = (message: messageError) => ({
    type: types.errorMessage,
    payload: message
});

export const removeErrorMessage = () => ({
    type: types.removeErrorMessage
});