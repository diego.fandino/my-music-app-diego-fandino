import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { addFavoriteSongs, removeFromFavoriteSongs } from '../../../actions/songs';
import { RootState } from '../../../store/store';
import { ImageSong } from '../../atoms/imageSong/ImageSong'
import { TextSongs } from '../../atoms/textSongs/TextSongs';
import './styles.scss';

interface rowSong {
  nameAlbum: string;
  imageAlbum: string;
  nameSong: string;
  artistName: string;
  duration: string;
  idSong: string;
}

export const RowSongs: React.FC<rowSong> = ({ nameAlbum, imageAlbum, nameSong, artistName, duration, idSong }) => {
  
  const dispatch = useDispatch();
  const { favoriteSongs } = useSelector((state: RootState) => state.songs);
  const [favoriteArray, setFavoriteArray] = useState<any[]>([]);

  useEffect(() => {
        let arrayTempFavorites = favoriteSongs.map( (favorite: any) => favorite.id );
        setFavoriteArray(arrayTempFavorites);
  }, [])

  const handleAddOrRemoveFavorites = (id: string, option: string) => {
      if(option === 'add'){
            dispatch(addFavoriteSongs(id));
            setFavoriteArray([...favoriteArray, id]);
      }else {
             dispatch(removeFromFavoriteSongs(id));
             let temporaryArrayRemove = favoriteArray.filter((idFavorite) => idFavorite !== id);
             setFavoriteArray(temporaryArrayRemove);
       } 
  }
  
      
  return (
    <div aria-label='itemSong' className='m-itemSong'>
        <div className='m-itemSong__item m-itemSong__item--responsive'>
              <ImageSong url={imageAlbum} name={nameAlbum} />
              <div className='m-itemSong__item__responsive'>
              <TextSongs text={nameSong} color='white' />
              <TextSongs text={artistName} color='grey' />
              </div>
        </div>
        <div className='m-itemSong__item m-itemSong__item--dissapear'>
              <TextSongs text={artistName} color='grey' />
        </div>
        <div className='m-itemSong__item m-itemSong__item--dissapear'>
              <TextSongs text={nameAlbum} color='grey' />
        </div>
        <div className='m-itemSong__item m-itemSong__item--favorite'>
              { (favoriteArray.indexOf(idSong) !== -1) ?  <i aria-label='favorite' onClick={() => handleAddOrRemoveFavorites(idSong,'remove')} className="bi bi-heart-fill m-itemSong__icon"></i> : <i onClick={() => handleAddOrRemoveFavorites(idSong,'add')} className='bi bi-heart m-itemSong__icon'></i>
               }
        </div>
        <div className='m-itemSong__item'>
              <TextSongs text={duration} color='grey' />
        </div>
    </div>
  )
}
