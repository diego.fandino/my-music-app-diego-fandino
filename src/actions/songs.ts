import { types } from "../types/types";
import axios from 'axios';
import { Dispatch } from "redux";
import Swal from "sweetalert2";

const url = 'https://api.spotify.com/v1/';

export const loadSongs =  () => {
    return async (dispatch: Dispatch) => {
        
        const tokenSpotify = localStorage.getItem('tokenSpotify');
        if(tokenSpotify !== null || tokenSpotify !== undefined){
            
            await axios.get(`${url}recommendations?seed_artists=4NHQUGzhtTLFvgF5SZesLK&seed_genres=salsa&seed_tracks=0c6xIDDpzE81m2q797ordA`, 
            { headers: { 'Authorization': 'Bearer ' + JSON.parse(tokenSpotify || '{}'),} })
                    .then(({data}: any) => {
                        dispatch(songsLoaded(data.tracks));
                    })
                    .catch((error) => {
                        Swal.fire('Error', 'Error to load songs from spotify...', 'error');
                    });
        }
        
    }

}


export const loadFavoriteSongs =  () => {
    return async (dispatch: Dispatch) => {
        
        const tokenSpotify = localStorage.getItem('tokenSpotify');
        if(tokenSpotify !== null || tokenSpotify !== undefined){
            
            await axios.get(`${url}me/tracks`, 
            { headers: { 'Authorization': 'Bearer ' + JSON.parse(tokenSpotify || '{}'),} })
                    .then(({data}: any) => {
                        
                        const arrayTracks = data.items.map( (item: any) => {
                            return item.track
                        })
                        dispatch(songsFavoritedLoaded(arrayTracks));
                    })
                    .catch((error) => {
                        console.log(error)
                        Swal.fire('Error', 'Error to load your favorite songs from spotify...', 'error');
                    });
        }
        
    }

}

export const addFavoriteSongs =  (idSong: string) => {
    return async () => {
        
        const tokenSpotify = localStorage.getItem('tokenSpotify');
        if(tokenSpotify !== null || tokenSpotify !== undefined){
            
            await axios.put(`${url}me/tracks`, {ids: [idSong]} ,
            { headers: { 'Authorization': 'Bearer ' + JSON.parse(tokenSpotify || '{}'),} })
                    .then()
                    .catch(() => {
                        Swal.fire('Error', 'Error to add this song to favorites...', 'error');
                    });
        }
        
    }

}

export const removeFromFavoriteSongs =  (idSong: string) => {
    return async () => {
        
        const tokenSpotify = localStorage.getItem('tokenSpotify');
        if(tokenSpotify !== null || tokenSpotify !== undefined){
            
            await axios.delete(`${url}me/tracks`, 
            { headers: { 'Authorization': 'Bearer ' + JSON.parse(tokenSpotify || '{}'),},
               data: [idSong]  })
                    .then()
                    .catch(() => {
                        Swal.fire('Error', 'Error to add this song to favorites...', 'error');
                    });
        }
        
    }

}

export const songsLoaded = (songs: any[]) => ({
    type: types.getAllSongOfPlaylist,
    payload: songs
});

export const songsFavoritedLoaded = (songs: any[]) => ({
    type: types.getFavoriteSongs,
    payload: songs
});