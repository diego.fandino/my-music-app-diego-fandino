import { types } from "../types/types"

const initialState = {
    token: null,
    refreshToken: null,
}

export const spotifyReducer = (state = initialState, action: any) => {

    switch (action.type) {
        case types.tokenSpotify:
            return {...state, token: action.payload}
        case types.checkTokenSpotify:
            return {...state, token: action.payload.token, refreshToken: action.payload.refreshToken}
        case types.refreshSpotify:
            return {...state, refreshToken: action.payload}
        case types.removeSpotifyCredentials:
            return {...state, refreshToken: null, token: null }
        default:
            return state;
    }

}

