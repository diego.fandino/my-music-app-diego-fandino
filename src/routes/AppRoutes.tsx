import React, { useEffect } from 'react'
import {  useDispatch, useSelector } from 'react-redux';
import {
    Routes,
    Route,
    BrowserRouter,
    Navigate,
  } from "react-router-dom";
import { checkLogin } from '../actions/auth';
import { LoginPage } from '../pages/login/LoginPage';
import { RegisterPage } from '../pages/register/RegisterPage';
import { RootState } from '../store/store';
import { DashboardRoutes } from "./DashboardRoutes";
import logoLoad from "../assets/logo_spotify_loginAndRegister.png";
import { checkingTokenSpotify } from '../actions/spotify';

export const AppRoutes = () => {

  const dispatch = useDispatch();
  const user = useSelector((state: RootState) => state.auth);
  const { loading = true } = useSelector((state: RootState) => state.loading);


  useEffect(() => {
    dispatch(checkLogin());
      dispatch(checkingTokenSpotify());
  }, [dispatch])
  

  return (
    <BrowserRouter>
        { loading ?
          <div className='loading'> 
            <img className='loading__logo' src={logoLoad} alt="spotify_cargando" />
            <span className='loading__text'>Cargando...</span> 
          </div> :
        <Routes>
            <Route path='/login' element={user.isLoggedIn ? <Navigate to='/' /> : <LoginPage />} />
            <Route path='/register' element={user.isLoggedIn ? <Navigate to='/' /> : <RegisterPage />} />
            <Route path="/*" element={user.isLoggedIn ? <DashboardRoutes /> : <LoginPage />} />
        </Routes>
        }
    </BrowserRouter>
       
  )
}
