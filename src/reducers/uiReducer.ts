import { types } from "../types/types";

const initialState = {
    loading: false,
    msgError: null
}

export const uiReducer = (state = initialState, action: any) => {

    switch (action.type) {
        case types.errorMessage:
            return {...state, msgError: action.payload }            
        case types.removeErrorMessage:
            return {...state, msgError: null }            
        default:
            return state
    }

}