import { types } from "../types/types";

const initialState = {
    loading: false
}

export const loadingReducer = (state = initialState, action: any) => {

    switch (action.type) {
        case types.startLoading:
            return {...state, loading: true}            
        case types.endLoading:
            return {...state, loading: false}            
    
        default:
            return state
    }

}