import React from 'react'
import "./styles.scss";

interface colorDividers {
    colorDivider?: String;
}
/**
 * 
 * @param {object} properties - Here only has one propertie
 * @param {string} colorDivider - It's could be 'dark' or 'light'
 * @returns {*} return a divider with it's properties 
 */

export const Divider: React.FC<colorDividers> = ({ colorDivider = 'dark' }) => {
  return (
    <div className={`a-divider ${colorDivider}`}></div>
  )
}
