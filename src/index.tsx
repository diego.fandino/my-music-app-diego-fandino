import React from 'react';
import ReactDOM from 'react-dom';
import { MusicApp } from "./MusicApp";
import {Provider} from 'react-redux';
import {store} from './store/store';
import  "./index.scss";
import '../src/utils/axiosInterceptor'


ReactDOM.render(
  <Provider store={store}>
    <MusicApp />
  </Provider>,
  document.getElementById('root')
);
