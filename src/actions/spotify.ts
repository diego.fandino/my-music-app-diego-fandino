import { Dispatch } from 'redux'
import { types } from "../types/types";
import axios from 'axios';
import {Buffer} from 'buffer';
import Swal from 'sweetalert2';
import { endLoading, startLoading } from './loading';



export const proccessTokenSpotify = (token: string) => {
    return (dispatch: Dispatch) => {
        if(token === undefined || token === null){
            return;
        }

        dispatch(startLoading());
        
        const data = new URLSearchParams();
        data.append("grant_type", "authorization_code");
        data.append("redirect_uri", (process.env.REACT_APP_SPOTIFY_REDIRECT_URI || '')?.toString());
        data.append("code", token);
        
        axios.post( 'https://accounts.spotify.com/api/token', 
        data,
        {
            headers: {
                'Authorization': 'Basic ' + Buffer.from(`${process.env.REACT_APP_SPOTIFY_CLIENT_ID}:${process.env.REACT_APP_SPOTIFY_CLIENT_SECRET}`).toString('base64'),
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }
        ).then(({data}) => {
            localStorage.setItem('tokenSpotify', JSON.stringify(data.access_token))
            localStorage.setItem('refreshTokenSpotify', JSON.stringify(data.refresh_token))
            dispatch(saveTokenSpotify(data.access_token));
            dispatch(refreshSpotify(data.refresh_token));
            dispatch(endLoading());
        })
        .catch((err) => {
            Swal.fire('Error', 'Error during generating of token...', 'error');
            dispatch(endLoading());
        })

    }
}

export const checkingTokenSpotify = () => {
    return (dispatch: Dispatch) => {
        const token = localStorage.getItem('tokenSpotify');
        const refreshToken = localStorage.getItem('refreshTokenSpotify');
        if(!!token && !!refreshToken){
            dispatch(checkTokenAndRefreshSpotify(JSON.parse(token), JSON.parse(refreshToken)));
        } else {
            return;
        }
    }
}

export const saveTokenSpotify = (token: string) => ({
    type: types.tokenSpotify,
    payload: token
})

export const checkTokenAndRefreshSpotify = (token: string, refreshToken: string) => ({
    type: types.checkTokenSpotify,
    payload: {
        token,
        refreshToken
    }
})

export const refreshSpotify = (token: string) => ({
    type: types.refreshSpotify,
    payload: token
})

export const removeCredentialsSpot = () => ({
    type: types.removeSpotifyCredentials
})