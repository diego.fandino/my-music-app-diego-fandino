import { types } from "../types/types"

const initialState = {
    songs: [],
    songActive: {},
    favoriteSongs: []
}

export const songsReducer = ( state = initialState, action: any ) => {

    switch (action.type) {
        case types.getAllSongOfPlaylist:
            return {...state, songs: action.payload}

        case types.getFavoriteSongs:
            return {...state, favoriteSongs: action.payload}
        
        case types.removeSongs:
            return initialState
        
        default:
            return state
    }

}