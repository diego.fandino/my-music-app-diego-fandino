import React from 'react'
import userIcon from "../../../assets/user_icon.png";
import "./styles.scss";

export const PhotoCircle = () => {
  return (
    <img src={userIcon} alt="user_icon" className='a-photo'/>
  )
}
