import React from 'react'
import {RegisterPage} from "./RegisterPage";
import {act, cleanup, fireEvent, render, screen, waitFor} from '@testing-library/react'
import { Provider, useSelector } from 'react-redux';
import { store } from '../../store/store';

const mockedNavigator = jest.fn();

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom') as any,
  useNavigate: () => ({
    navigate: mockedNavigator
  }),
}));


describe('Here we will doing tests in register component', () => {


    beforeEach(() => {
        store.dispatch = jest.fn();
    })
    
    afterEach(cleanup);

    it('Name input field must be Required', async () => { 

        render( 
            <Provider store={store}>
                <RegisterPage />
            </Provider>
        );

        await act( async () => {
            fireEvent.change( screen.getByLabelText('name'), { target: {value: ''} } );
            fireEvent.change( screen.getByLabelText('email'), { target: {value: 'diegofandino@gmail.com'} } );
            fireEvent.change( screen.getByLabelText('password'), { target: {value: '123456'} } );
        });

        await act( async () => {
            fireEvent.click(screen.getByRole('button', {name: /register/i}));
        });
        
        await waitFor(() => {
            const spanElement = screen.getByLabelText('ErrorMessages').textContent;
            expect( spanElement).toBe('This field is required');
        })


    });
    
    it('Email input field must be Required', async () => { 

        render( 
            <Provider store={store}>
                <RegisterPage />
            </Provider>
        );

        await act( async () => {
            fireEvent.change( screen.getByLabelText('name'), { target: {value: 'Diego'} } );
            fireEvent.change( screen.getByLabelText('email'), { target: {value: ''} } );
            fireEvent.change( screen.getByLabelText('password'), { target: {value: '123456'} } );
        });
        
        await act( async () => {
            fireEvent.click(screen.getByRole('button', {name: /register/i}));
        });
        
        await waitFor(() => {
            const spanElement = screen.getByLabelText('ErrorMessages').textContent;
            expect( spanElement).toBe('This field is required');
        })
        
        
    });
    
    it('Password input field must be Required', async () => { 
        
        render( 
                <Provider store={store}>
                    <RegisterPage />
                </Provider>
            );
            
            await act( async () => {
            fireEvent.change( screen.getByLabelText('name'), { target: {value: 'Diego'} } );
            fireEvent.change( screen.getByLabelText('email'), { target: {value: 'diegofandino@gmail.com'} } );
            fireEvent.change( screen.getByLabelText('password'), { target: {value: ''} } );
        });
        
        await act( async () => {
            fireEvent.click(screen.getByRole('button', {name: /register/i}));
        });
        
        await waitFor(() => {
            const spanElement = screen.getByLabelText('ErrorMessages').textContent;
            expect( spanElement).toBe('This field is required');
        })
        
        
    });
    
    it('Email input field must be email type', async () => { 
        
        render( 
            <Provider store={store}>
                <RegisterPage />
            </Provider>
        );
        
        await act( async () => {
            fireEvent.change( screen.getByLabelText('name'), { target: {value: 'Diego'} } );
            fireEvent.change( screen.getByLabelText('email'), { target: {value: 'newEmail'} } );
            fireEvent.change( screen.getByLabelText('password'), { target: {value: '123456'} } );
        });
        
        await act( async () => {
            fireEvent.click(screen.getByRole('button', {name: /register/i}));
        });
        
        await waitFor(() => {
            const spanElement = screen.getByLabelText('ErrorMessages').textContent;
            expect( spanElement).toBe('Invalid email address');
        })
        
        
    });
    
    it('Correct form with valid inputs', async () => { 
        render( 
            <Provider store={store}>
                <RegisterPage />
            </Provider>
        );
        
        
        await act( async () => {
            fireEvent.change( screen.getByLabelText('name'), { target: {value: 'Diego'} } );
            fireEvent.change( screen.getByLabelText('email'), { target: {value: 'diegofandino@gmail.com'} } );
            fireEvent.change( screen.getByLabelText('password'), { target: {value: '123456'} } );
        });

        await act( async () => {
            fireEvent.click(screen.getByRole('button', {name: /register/i}));
        });

        expect(store.dispatch).toHaveBeenCalledTimes(1);

    });

   

});