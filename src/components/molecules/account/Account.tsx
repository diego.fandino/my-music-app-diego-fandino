import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../../../store/store'
import { PhotoCircle } from '../../atoms/photoCircle/PhotoCircle'
import { Dropdown } from 'react-bootstrap';
import "./styles.scss";
import { processLogout } from '../../../actions/auth';

export const Account = () => {

    const dispatch = useDispatch();
    const { user } = useSelector( (state: RootState) => state.auth );

    const handleLogout = () => {
      dispatch(processLogout());
    }

  return (

    <Dropdown className='o-header__account'>
      <Dropdown.Toggle variant="success" id="dropdown-basic">
        <div className='dropdown' data-bs-toggle="dropdown" id="dLabel">
          <PhotoCircle />
          <span className='o-header__account__user'> { user.name } </span>
      </div>
      </Dropdown.Toggle>

      <Dropdown.Menu variant="dark" className='o-header__account__logout'>
        <Dropdown.Item onClick={handleLogout}>Logout</Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>

  )
}
