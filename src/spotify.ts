const authEndpoint = "https://accounts.spotify.com/authorize?";
export const apiToken = "https://accounts.spotify.com/api/token";

const scopes = [
    'user-read-playback-state',
    'user-modify-playback-state',
    'user-read-private',
    'user-follow-modify',
    'user-library-modify',
    'user-library-read',
    'playlist-modify-private',
    'playlist-read-private',
    'playlist-modify-public',
    'user-read-currently-playing',
    'user-read-recently-played'
]

export const loginEndpoint = `${authEndpoint}client_id=${process.env.REACT_APP_SPOTIFY_CLIENT_ID}&redirect_uri=${process.env.REACT_APP_SPOTIFY_REDIRECT_URI}&scope=${scopes.join('%20')}&response_type=code`;