import React, { useEffect, useState } from 'react'
import "./styles.scss";
import { NavLink } from 'react-router-dom';

interface items {
    text: string;
    keyNumber: number;
    icon: string;
}

const getWindowDimensions = () => {
  const { innerWidth: width } = window;
  return {
    width
  };
}

export const ItemsMenu: React.FC<items> = ({ text, icon, keyNumber }) => {

  const [windowDimensions, setWindowDimensions] = useState(getWindowDimensions());

  useEffect(() => {
    const handleResize = () => {
      setWindowDimensions(getWindowDimensions());
    }
    window.addEventListener('resize', handleResize);
    return () => window.removeEventListener('resize', handleResize);
  }, [windowDimensions]);

  return (
    
    <li key={keyNumber.toString()} className='a-item'>
        <NavLink
            to={text === 'home' ? "/" : text}
            className={({ isActive }) =>
            isActive ? 'a-item__text' + '--active' : 'a-item__text'
            }
            > 
            { windowDimensions.width >= 780 ? text : 
              <i className={`bi bi-${icon} a-item__a-icons`}></i>
            } 
              <div className="barActive"></div>
        </NavLink>
    </li>
  )
}
