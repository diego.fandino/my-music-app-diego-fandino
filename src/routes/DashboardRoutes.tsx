import React from 'react'
import {
    Routes,
    Route,
  } from "react-router-dom";
import { FavoriteSongs } from '../pages/favorites/FavoriteSongs';
import { HomePage } from '../pages/home/HomePage';
import { Songs } from '../pages/songs/Songs';

export const DashboardRoutes = () => {
  return (
    <Routes>
        <Route path="/" element={<HomePage />} >
          <Route index element={<Songs />} />
          <Route path="favorites" element={<FavoriteSongs />} />
        </Route>
    </Routes>
  )
}
