import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { loadFavoriteSongs } from '../../actions/songs';
import { TopInformation } from '../../components/molecules/topPageInformation/TopInformation'
import { ListSongs } from '../../components/organisms/listSongs/ListSongs';
import { RootState } from '../../store/store';

export const FavoriteSongs = () => {

  const dispatch = useDispatch();
  const { favoriteSongs } = useSelector((state: RootState) => state.songs);

  useEffect(() => {
      dispatch(loadFavoriteSongs());
  }, [])
  

  return (
    <>
        <TopInformation title='Your favorites!' subtitle={`Find all your favorite songs here.`} />
        <ListSongs songsImport={favoriteSongs}/>
    </>
  )
}
