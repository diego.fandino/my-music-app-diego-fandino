import React from 'react'
import './styles.scss';

interface textGeneral {
    text: string;
    color: string;
}

export const TextSongs: React.FC<textGeneral> = ( {text, color} ) => {
  return (
    <span className={`a-textGeneral a-textGeneral--${color}`} > {text} </span>
  )
}
