import {  combineReducers  } from "redux";
import { authReducer } from "./authReducer";
import { loadingReducer } from "./loadingReducer";
import { songsReducer } from "./songsReducer";
import { spotifyReducer } from "./spotifyReducers";
import { uiReducer } from "./uiReducer";

export const reducers = combineReducers({
    auth: authReducer,
    spotify: spotifyReducer,
    loading: loadingReducer,
    songs: songsReducer,
    ui: uiReducer
});