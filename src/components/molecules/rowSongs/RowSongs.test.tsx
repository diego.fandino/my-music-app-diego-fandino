import React from 'react'
import {  act, fireEvent, render, screen, waitFor } from '@testing-library/react';
import { Provider } from 'react-redux';
import { store } from "../../../store/store";
import { ListSongs } from '../../organisms/listSongs/ListSongs';
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk';

const mockStore = configureMockStore([thunk]);

describe('In this test we need get favorites songs', () => { 

    
    const mockedData = [
        {
            disc_number: 1,
            duration_ms: 213211,
            explicit: false,
            href: "https://api.spotify.com/v1/tracks/2uMbCJ8lxF7hqpQUaeo42D",
            id: "2uMbCJ8lxF7hqpQUaeo42D",
            is_local: false,
            name: "Do It Right",
            popularity: 64,
            preview_url: null,
            track_number: 1,
            type: "track",
            uri: "spotify:track:2uMbCJ8lxF7hqpQUaeo42D",
            album: {
                album_type: "SINGLE",
                href: "https://api.spotify.com/v1/albums/0O9TE4lPPsojRsKkWqByVn",
                id: "0O9TE4lPPsojRsKkWqByVn",
                name: "Do It Right",
                release_date: "2016-05-20",
                release_date_precision: "day",
                total_tracks: 1,
                type: "album",
                uri: "spotify:album:0O9TE4lPPsojRsKkWqByVn",
                images: [
                    {height: 640,
                        url: "https://i.scdn.co/image/ab67616d0000b273090f4afcd7663f4ccdecbb01",
                        width: 640},
                    {height: 300,
                        url: "https://i.scdn.co/image/ab67616d00001e02090f4afcd7663f4ccdecbb01",
                        width: 300},
                    {height: 64,
                        url: "https://i.scdn.co/image/ab67616d00004851090f4afcd7663f4ccdecbb01",
                        width: 64},
                ]
            },
            artists: [
                {
                    href:"https://api.spotify.com/v1/artists/61lyPtntblHJvA7FMMhi7E",
                    id:"61lyPtntblHJvA7FMMhi7E",
                    name:"Duke Dumont",
                    type:"artist",
                    uri:"spotify:artist:61lyPtntblHJvA7FMMhi7E",

                }
            ] 
        },
        {
            disc_number:1,
            duration_ms:389740,
            explicit:false,
            href:"https://api.spotify.com/v1/tracks/7r3Yg7Jl8tIfCryEaaYf41",
            id:"7r3Yg7Jl8tIfCryEaaYf41",
            is_local:false,
            name:"The Giver",
            popularity:39,
            preview_url:null,
            track_number:3,
            type:"track",
            uri:"spotify:track:7r3Yg7Jl8tIfCryEaaYf41",
            album: {
                album_type:"SINGLE",
                href:"https://api.spotify.com/v1/albums/5ZzT6EX1TinZGrS7CuaTHI",
                id:"5ZzT6EX1TinZGrS7CuaTHI",
                name:"EP1",
                release_date:"2014-01-01",
                release_date_precision:"day",
                total_tracks:4,
                type:"album",
                uri:"spotify:album:5ZzT6EX1TinZGrS7CuaTHI",
                images: [
                    {height:640,
                        url:"https://i.scdn.co/image/ab67616d0000b273e197a220029b6b6945ae0426",
                        width:640},
                    {height:300,
                        url:"https://i.scdn.co/image/ab67616d00001e02e197a220029b6b6945ae0426",
                        width:300},
                    {height:64,
                        url:"https://i.scdn.co/image/ab67616d00004851e197a220029b6b6945ae0426",
                        width:64},
                ]
            },
            artists: [
                {
                    href:"https://api.spotify.com/v1/artists/1bj5GrcLom5gZFF5t949Xl",
                    id:"1bj5GrcLom5gZFF5t949Xl",
                    name:"Martin Solveig",
                    type:"artist",
                    uri:"spotify:artist:1bj5GrcLom5gZFF5t949Xl",
                    
                }
            ] 
        },
        {
            available_markets: [],
            disc_number:1,
            duration_ms:170319,
            explicit:false,
            href:"https://api.spotify.com/v1/tracks/1dNyeO5Oy7RuGYOq30HrK2",
            id:"1dNyeO5Oy7RuGYOq30HrK2",
            is_local:false,
            name:"Night Call",
            popularity:63,
            preview_url:null,
            track_number:3,
            type:"track",
            uri:"spotify:track:1dNyeO5Oy7RuGYOq30HrK2",
            album: {
                album_type:"album",
                available_markets: [],
                href:"https://api.spotify.com/v1/albums/4ANoIvV1JMAne7lFSExMI0",
                id:"4ANoIvV1JMAne7lFSExMI0",
                name:"Night Call (New Year's Edition)",
                release_date:"2022-01-21",
                release_date_precision:"day",
                total_tracks:23,
                type:"album",
                uri:"spotify:album:4ANoIvV1JMAne7lFSExMI0",
                images: [
                    {height:640,
                        url:"https://i.scdn.co/image/ab67616d0000b273d5bc8820acb21f8914e70fda",
                        width:640},
                    {height:300,
                        url:"https://i.scdn.co/image/ab67616d00001e02d5bc8820acb21f8914e70fda",
                        width:300},
                    {height:64,
                        url:"https://i.scdn.co/image/ab67616d00004851d5bc8820acb21f8914e70fda",
                        width:64},
                ]
            },
            artists: [
                {
                    href:"https://api.spotify.com/v1/artists/5vBSrE1xujD2FXYRarbAXc",
                    id:"5vBSrE1xujD2FXYRarbAXc",
                    name:"Years & Years",
                    type:"artist",
                    uri:"spotify:artist:5vBSrE1xujD2FXYRarbAXc",
                }
            ] 
        }
    ]
    
    const mockedDataFavorites = [
        {
            disc_number:1,
            duration_ms:389740,
            explicit:false,
            href:"https://api.spotify.com/v1/tracks/7r3Yg7Jl8tIfCryEaaYf41",
            id:"7r3Yg7Jl8tIfCryEaaYf41",
            is_local:false,
            name:"The Giver",
            popularity:39,
            preview_url:null,
            track_number:3,
            type:"track",
            uri:"spotify:track:7r3Yg7Jl8tIfCryEaaYf41",
            album: {
                album_type:"SINGLE",
                href:"https://api.spotify.com/v1/albums/5ZzT6EX1TinZGrS7CuaTHI",
                id:"5ZzT6EX1TinZGrS7CuaTHI",
                name:"EP1",
                release_date:"2014-01-01",
                release_date_precision:"day",
                total_tracks:4,
                type:"album",
                uri:"spotify:album:5ZzT6EX1TinZGrS7CuaTHI",
                images: [
                    {height:640,
                        url:"https://i.scdn.co/image/ab67616d0000b273e197a220029b6b6945ae0426",
                        width:640},
                    {height:300,
                        url:"https://i.scdn.co/image/ab67616d00001e02e197a220029b6b6945ae0426",
                        width:300},
                    {height:64,
                        url:"https://i.scdn.co/image/ab67616d00004851e197a220029b6b6945ae0426",
                        width:64},
                ]
            },
            artists: [
                {
                    href:"https://api.spotify.com/v1/artists/1bj5GrcLom5gZFF5t949Xl",
                    id:"1bj5GrcLom5gZFF5t949Xl",
                    name:"Martin Solveig",
                    type:"artist",
                    uri:"spotify:artist:1bj5GrcLom5gZFF5t949Xl",
                    
                }
            ] 
        },
        {
            available_markets: [],
            disc_number:1,
            duration_ms:170319,
            explicit:false,
            href:"https://api.spotify.com/v1/tracks/1dNyeO5Oy7RuGYOq30HrK2",
            id:"1dNyeO5Oy7RuGYOq30HrK2",
            is_local:false,
            name:"Night Call",
            popularity:63,
            preview_url:null,
            track_number:3,
            type:"track",
            uri:"spotify:track:1dNyeO5Oy7RuGYOq30HrK2",
            album: {
                album_type:"album",
                available_markets: [],
                href:"https://api.spotify.com/v1/albums/4ANoIvV1JMAne7lFSExMI0",
                id:"4ANoIvV1JMAne7lFSExMI0",
                name:"Night Call (New Year's Edition)",
                release_date:"2022-01-21",
                release_date_precision:"day",
                total_tracks:23,
                type:"album",
                uri:"spotify:album:4ANoIvV1JMAne7lFSExMI0",
                images: [
                    {height:640,
                        url:"https://i.scdn.co/image/ab67616d0000b273d5bc8820acb21f8914e70fda",
                        width:640},
                    {height:300,
                        url:"https://i.scdn.co/image/ab67616d00001e02d5bc8820acb21f8914e70fda",
                        width:300},
                    {height:64,
                        url:"https://i.scdn.co/image/ab67616d00004851d5bc8820acb21f8914e70fda",
                        width:64},
                ]
            },
            artists: [
                {
                    href:"https://api.spotify.com/v1/artists/5vBSrE1xujD2FXYRarbAXc",
                    id:"5vBSrE1xujD2FXYRarbAXc",
                    name:"Years & Years",
                    type:"artist",
                    uri:"spotify:artist:5vBSrE1xujD2FXYRarbAXc",
                }
            ] 
        }
    ]

    test('Show with aria label the favorites songs', async () => { 
        
            jest.mock('react-redux', () => ({
                ...jest.requireActual('react-redux'),
                useSelector: jest.fn(),
                useDispatch: jest.fn()
            }));
    
            const store = mockStore({
                songs: { favoriteSongs: [...mockedDataFavorites] },
            });

            render(
                <Provider store={store}>
                    <ListSongs songsImport={mockedData} />
                </Provider>
            )

            const div = screen.findAllByLabelText('favorite');
            expect(  (await div).length ).toBeGreaterThanOrEqual(1)
            
        })
        
        test('Test the button to remove favorites', async () => { 
            
            const handleAddOrRemoveFavorites = jest.fn();
            
            jest.mock('react-redux', () => ({
                ...jest.requireActual('react-redux'),
                useSelector: jest.fn(),
                useDispatch: jest.fn()
            }));
            
            const store = mockStore({
                songs: { favoriteSongs: [...mockedDataFavorites] },
            });
            
            
            render(
                <Provider store={store}>
                    <ListSongs songsImport={mockedData} />
                </Provider>
            )
            
            //After event click
            const favorites = screen.getAllByLabelText('favorite');
            
            await act( async () => {
                fireEvent.click( favorites[1] );
            });
            
            waitFor( async () => {
                const div = screen.findAllByLabelText('favorite');
                expect(  (await div).length ).toBe(1)
            })
            

     })


})