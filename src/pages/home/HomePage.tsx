import React, { useEffect, useState } from 'react'
import { Outlet } from 'react-router-dom';
import { Header } from '../../components/organisms/header/Header';
import "./styles.scss";

export const HomePage = () => {

  const [classFixed, setclassFixed] = useState('');

  useEffect(() => {

    const returnHeight = () => {
      window.scrollY > 90 ? setclassFixed('--sticky') : setclassFixed(''); 
    }
    window.addEventListener('scroll', returnHeight);
    return () => window.removeEventListener('scroll', returnHeight);
}, [window.scrollY])

  return (
    <main className={`contentApp contentApp${classFixed}`}>
        <Header />
        <div className="container contentApp__padding">
          <Outlet />
        </div>
    </main>
    
  )
}
