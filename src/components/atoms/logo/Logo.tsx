import React from 'react'
import { useNavigate } from 'react-router-dom';
import logo from '../../../assets/logo_home.png'
import "./styles.scss";

interface navigateHome {
  navigateHome: boolean
}

export const Logo: React.FC<navigateHome> = ( {navigateHome} ) => {

  let navigate = useNavigate();
  
  const handleHome = (navigateHome: boolean) => {
      if(navigateHome){
        navigate('/');
      }
  }

  return (
    <img onClick={() => handleHome(navigateHome)} className='a-logo' src={logo} alt='logo_spotifyHome' />
  )
}
