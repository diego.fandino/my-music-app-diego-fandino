import React, { useEffect  } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { loadSongs } from '../../actions/songs'
import { proccessTokenSpotify } from '../../actions/spotify'
import { TopInformation } from '../../components/molecules/topPageInformation/TopInformation'
import { ListSongs } from '../../components/organisms/listSongs/ListSongs'
import { RootState } from '../../store/store'

export const Songs = () => {

  const dispatch = useDispatch();
  const { songs } = useSelector((state: RootState) => state.songs);
  const { token } = useSelector((state: RootState) => state.spotify);

  
  
  useEffect(() => {
    
    if(localStorage.getItem('tokenSpotify') === '' || localStorage.getItem('tokenSpotify') === null || localStorage.getItem('tokenSpotify') === undefined){
      const search = window.location.search;
      dispatch(proccessTokenSpotify(search.split('=')[1]));
    }
    
  }, []);
  
  useEffect(() => {
    if(token){
      dispatch(loadSongs());
    }
  }, [token])
  
  

  return (
    <>
      <TopInformation title='Listen to your music!' subtitle={`Find all your favorite songs, new playlists, songs to discover and much more!`} />
      <ListSongs songsImport={songs}/>
      {/* <a href={loginEndpoint} rel="noopener noreferrer"> LOGIN SPOTIYF </a> */}
    </>
  )
}
