import axios from 'axios';
import { store } from '../store/store';
import { types } from '../types/types';
import {Buffer} from 'buffer';


const interceptor = axios.interceptors.response.use(
    (response) => { 
      return response;
    },
    (error) => {
      // Do something with response error
      // Store the original request
      const originalReq = error.config;

      if (error.response.status !== 401) {
        return Promise.reject(error);
    }

          // Set the retry flag to true
            originalReq._retry = true;

            axios.interceptors.response.eject(interceptor);

            const refreshToken = localStorage.getItem('refreshTokenSpotify');

            if(!!refreshToken){

                const data = new URLSearchParams();
                data.append("grant_type", "refresh_token");
                data.append("refresh_token", JSON.parse(refreshToken));
                
                return axios.post( 'https://accounts.spotify.com/api/token', 
                data,
                {
                    headers: {
                        'Authorization': 'Basic ' + Buffer.from(`${process.env.REACT_APP_SPOTIFY_CLIENT_ID}:${process.env.REACT_APP_SPOTIFY_CLIENT_SECRET}`).toString('base64'),
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                }
                ).then(({data}) => {
                    localStorage.setItem('tokenSpotify', JSON.stringify(data.access_token))
                    store.dispatch({
                        type: types.tokenSpotify,
                        payload: data.access_token});
                    // return axios(originalReq);
                    return Promise.resolve(true);
                })
                .catch((err) => {
                    store.dispatch({type: types.authLogout});
                    return Promise.reject(error);
                })
            }
    });