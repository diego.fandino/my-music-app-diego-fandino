import React, { useEffect, useState } from 'react'
import { RowSongs } from '../../molecules/rowSongs/RowSongs';

interface songsImported {
    songsImport: any[]
}

export const ListSongs: React.FC<songsImported> = ({songsImport}) => {

    const [arraySongs, setArraySongs] = useState<any[]>(songsImport || [])

    const millisToMinutesAndSeconds = (millis: number) => {
        var minutes = Math.floor(millis / 60000);
        var seconds = ((millis % 60000) / 1000).toFixed(0);
        return minutes + ":" + (parseFloat(seconds) < 10 ? 0 : '') + seconds;
    }

    useEffect(() => {

        let songArray = songsImport.map((song: any) => {
            return {...song, durationSeconds: millisToMinutesAndSeconds(song.duration_ms)}
        })
    
        setArraySongs(songArray);
    }, [songsImport])
    
    


  return (
      <div className='o-allSongs'>
          {arraySongs.length > 0 && arraySongs.map( (song: any) => (
              <RowSongs key={song.id} idSong={song.id} nameAlbum={song.album?.name} imageAlbum={song.album?.images.length > 0 ? song.album?.images[2].url : ''} nameSong={song.name} artistName={song.artists[0].name} duration={song.durationSeconds} />
          ))}
      </div>
  )
}
