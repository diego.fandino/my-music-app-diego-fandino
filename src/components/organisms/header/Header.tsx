import React, { useEffect, useState } from 'react'
import { Logo } from '../../atoms/logo/Logo';
import { Account } from '../../molecules/account/Account';
import { Menu } from '../../molecules/menu/Menu';
import "./styles.scss";

interface itemTypes {
    id: number;
    title: string;
    icon: string;
}

export const Header = () => {

    let menuItems: itemTypes[] = [
        {id: 1, title: 'home', icon: 'house'},
        {id: 2, title: 'favorites', icon: 'star'},
    ] 
    const [itemsMenu, setItemsMenu] = useState(menuItems);
    const [classFixed, setclassFixed] = useState('');

    useEffect(() => {

        const returnHeight = () => {
          window.scrollY > 90 ? setclassFixed('--sticky') : setclassFixed(''); 
        }
        window.addEventListener('scroll', returnHeight);
        return () => window.removeEventListener('scroll', returnHeight);
    }, [window.scrollY])
    

  return (
    <header className={`o-header o-header${classFixed}`}>
        <Logo navigateHome={true} />
        <Menu items={itemsMenu} />
        <Account />
    </header>
  )
}
