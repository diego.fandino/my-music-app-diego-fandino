import React from 'react'

import { ItemsMenu } from '../../atoms/itemsMenu/ItemsMenu';
import "./styles.scss";

interface itemTypes {
    id: number;
    title: string;
    icon: string;
}

interface itemsMenu {
    items: itemTypes[];
}


export const Menu: React.FC<itemsMenu> = ({ items }) => {

  return (
    <nav>
        <ul className='m-menuItems'>
            {
                items.map( (item) => (
                      <ItemsMenu key={item.id} keyNumber={item.id} text={item.title} icon={item.icon} />
                ))
            }
        </ul>
    </nav>
  )
}
